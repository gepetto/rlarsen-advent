﻿Import-Module "../DayFour.psm1"
Import-Module Pester

Describe "Get-AdventHash"{
    It "Takes Secret Keys and finds the Advent Hash for sweet sweet Advent Coins"{
        Get-AdventHash "abcdef" | Should Be 609043
    }
    It "Mines me another Advent Coin Example"{
        "pqrstuv" | Get-AdventHash | Should Be 1048970
    }
}

Remove-Module Pester
Remove-Module DayFour
﻿Import-Module Hashing

Function Get-AdventHash{
    [cmdletbinding()]
    Param([Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [string]$key
    )
    process{
        $number = 0
        $hash = "0"
        while($hash -notlike "00000*"){
            $hash = $key + [string]$number | Get-Hash -algorithm MD5

            $number++
        }
        return ($number)-1
    }
}

Function Get-AdventierHash{
    [cmdletbinding()]
    Param([Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [string]$key
    )
    process{
        $number = 0
        $hash = "0"
        while($hash -notlike "000000*"){
            $hash = $key + [string]$number | Get-Hash -algorithm MD5

            $number++
        }
        return ($number)-1
    }
}
Remove-Module Hashing

Export-ModuleMember -Function Get-AdventHash
Export-ModuleMember -Function Get-AdventierHash
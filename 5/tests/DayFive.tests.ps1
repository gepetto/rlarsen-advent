﻿Import-Module "../DayFive.psm1"
Import-Module Pester

Describe "Get-NiceStringCount"{
    It "Takes input and tells how many nice strings there are"{
        Get-NiceStringCount "aaei" | Should Be 1
    }

}

Remove-Module Pester
Remove-Module DayFive
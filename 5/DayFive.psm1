﻿Function Get-NiceStringCount{
    [cmdletbinding()]
    Param([Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [string]$input
    )
    process{
    $vowels = "a","e","i","o","u"
    $vowel_count = 0
    $naughtstrings = "ab","cd","pq","xy"
    $nice_strings = @()
    
    #Loop through the input strings
    foreach($string in $input){
        #loop through the strings a nice string can't have
        foreach($naughty in $naughtstrings){
            Write-Host $naughty
            if($string -contains $naughty){
                #Stop processing current string if it contains any of the four strings
                break
            }else{
                #loop through vowels and count them
                foreach($vowel in $vowels){
                    Write-Host $vowel
                    if($string -contains $vowel){
                        $vowel_count++
                    }
                    Write-Host $vowel_count
                    if($vowel_count -eq 3){
                        foreach($letter in $string.ToCharArray()){
                            if($letter[$_] -like $letter[$_+1]){
                                $nice_strings += $string
                            }
                        }
                    }
                }
            }
        }
    }
    return $nice_strings.Length
    }
}

Export-ModuleMember -Function Get-NiceStringCount
﻿Import-Module "../DayThree.psm1"
Import-Module Pester

Describe "Get-DeliveryCount"{
    It "Gives Santa one direction, resulting in 2 unique houses"{
        Get-DeliveryCount ">" | Should Be 2
    }
    It "Gets the number of unique houses Santa visited"{
        Get-DeliveryCount -directions "^>v<" | Should Be 4
    }
    It "Sends Santa back and forth a bunch of times between two houses"{
        Get-DeliveryCount "^v^v^v^v^v" | Should be 2
    }
}
Describe "Get-DuelingSantas"{
    It "Sends Sanda and Robot Santa in opposite directions"{
        Get-DuelingSantas "^v" | Should Be 3
    }
    It "Sends Santa and Robot Santa each out and back"{
        Get-DuelingSantas "^>v<" | Should Be 3
    }
    It "Sends Santa and Robot Santa in opposite directions for a bit"{
        Get-DuelingSantas "^v^v^v^v^v" | Should Be 11
    } 
}

Remove-Module Pester
Remove-Module DayThree
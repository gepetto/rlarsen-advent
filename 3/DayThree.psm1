﻿Function Get-DeliveryCount{
    [cmdletbinding()]
    Param([Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [string]$directions
    )
    process{
        $house = @()
        $house += "1,1"
        $updown = 1
        $leftright = 1
        foreach($item in $directions.ToCharArray()){
            #switch($item){
            #    "<"{$leftright -= 1}
            #    ">"{$leftright += 1}
            #    "^"{$updown += 1}
            #    "v"{$updown -= 1}
            #}
            #if($house.Contains("$updown,$leftright")){
            #}else{
            #    $house += "$updown,$leftright"
            #}
            $house += (Santa $item $updown $leftright)[0]
        }
        return $house
    }

}
Function Get-DuelingSantas{
    [cmdletbinding()]
    Param([Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [string]$directions
    )
    process{
        $santa_house = @()
        $robot_house = @()
        $santa_house += "1,1"
        $robot_house += "1,1"
        $rb_vert = 1
        $rb_hori = 1
        $san_vert = 1
        $san_hori = 1
        [char[]]$directions = $directions.ToCharArray()
        for($item = 0; $item -le $directions.Count; $item++){
            if($item%2 -eq 0){
                #write-host (Santa $directions[$item] $santa_house $san_vert $san_hori)
                $santa_house = (Santa $directions[$item] $santa_house $san_vert $san_hori)[0]
                $san_vert = (Santa $directions[$item] $santa_house $san_vert $san_hori)[1]
                $san_hori = (Santa $directions[$item] $santa_house $san_vert $san_hori)[2]
            }else{
                #write-host (Santa $directions[$item] $robot_house $rb_vert $rb_hori)
                $robot_house = (Santa $directions[$item] $robot_house $rb_updown $rb_hori)[0]
                $rb_vert = (Santa $directions[$item] $robot_house $rb_updown $rb_hori)[1]
                $rb_hori = (Santa $directions[$item] $robot_house $rb_updown $rb_hori)[2]
            }
        }
        return ([array]$santa_house, [array]$robot_house)
    }

}

Function Santa($item, $updown, $leftright){ 
    switch($item){
        "<"{$leftright += -1}
        ">"{$leftright += 1}
        "^"{$updown += 1}
        "v"{$updown += -1}
    }
    if($house.Contains("$updown,$leftright")){
    }else{
        $house += "$updown,$leftright"
    }
    return $house, $updown, $leftright
}

Export-ModuleMember -Function Get-DuelingSantas
Export-ModuleMember -Function Get-DeliveryCount
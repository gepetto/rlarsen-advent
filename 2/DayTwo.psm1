﻿# advent of code, day 2

<# 
 .Synopsis
    Takes an input of instructions and returns the resulting floor
 .Description
    Day Two of adventofcode.com

    The elves have to figure out how much wrapping paper they need!
    Given a series of LxWxH, plus slack equal to the area of the smallest side

    Figure out how many square feet the elves need!

 .Parameter dimentions
    Dimentions are a series of lxwxh 

 .Example
    cat ".\input.txt" | Get-TotalSquareFeet
#>

Function Get-TotalSquareFeet{
    [cmdletbinding()]
    Param([Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [array]$dimensions
    )
    $total = 0
    foreach($box in $dimensions){
        $box = $box.split('x')
        $sides = @{'l' = [double]$box[0]; 'w' = [double]$box[1]; 'h' = [double]$box[2]}

        $squarefeet = GetSquareFeet($sides)
        $smallestside = GetSmallestSide($sides)

        $total += ($squarefeet + $smallestside)
    }
    return $total
}

Function Get-NeededRibbon{
    [cmdletbinding()]
    Param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        [array]$dimensions
    )
    $total = 0
    foreach($box in $dimensions){
        $box = $box.split('x')
        $sides = @{'l' = [double]$box[0]; 'w' = [double]$box[1]; 'h' = [double]$box[2]}

        $ribbonlength = GetRibbonLength($sides)
        $bowlength = GetBowLength($sides)

        $total += ($ribbonlength + $bowlength)
    }
    return $total
}

Function GetSmallestSide([hashtable]$sides){
    $smallest = @(($sides.'l' * $sides.'w'), ($sides.'w' * $sides.'h'), ($sides.'l' * $sides.'h'))
    return ($smallest | Sort-Object)[0]
}

Function GetSquareFeet([hashtable]$sides){
    $squarefeet = (($sides.'l' * $sides.'w') + ($sides.'w' * $sides.'h') + ($sides.'l' * $sides.'h')) * 2
    return $squarefeet
}

Function GetBowLength([hashtable]$sides){
    return $sides.'l' * $sides.'w' * $sides.'h'
}

Function GetRibbonLength([hashtable]$sides){
    $sides_arr = @($sides.'l', $sides.'w', $sides.'h')
    $smallest = ($sides_arr | Sort-Object)[0]
    $second_smallest = ($sides_arr | Sort-Object)[1]

    return ($smallest + $second_smallest)*2
}

Export-ModuleMember -Function Get-TotalSquareFeet
Export-ModuleMember -Function Get-NeededRibbon
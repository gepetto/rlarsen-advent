﻿Import-Module "../DayTwo.psm1"
Import-Module Pester

Describe "Get-TotalSquareFeet"{
    It "Totals Needed Square Feet of Wrapping Paper (Area + Smallest Side)"{
        Get-TotalSquareFeet -dimensions 2x2x2 | Should Be 28
    }
    It "Totals two sets of Dimensions"{
        Get-TotalSquareFeet -dimensions 2x2x2,2x2x2 | Should Be 56
    }
    It "Totals from a file as an argument"{
        Get-TotalSquareFeet -dimensions (Get-Content ..\input.txt) | Should Not Be 15
    }
}

Describe "Get-NeededRibbon"{
    It "totals Needed Feet of Ribbon to wrap box"{
        Get-NeededRibbon -dimensions 2x3x4 | Should Be 34
    }
    It "Runs the second example"{
        Get-NeededRibbon -dimensions 1x1x10 | Should Be 14
    }
    It "Gets needed ribbon from exercise file"{
        Get-NeededRibbon -dimensions (Get-Content ..\input.txt) | Should not be 15
    }
}
Remove-Module Pester
Remove-Module DayTwo
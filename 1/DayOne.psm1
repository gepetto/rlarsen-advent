﻿# advent of code, day 1
# solved in Python3 already, doing it in Powershell
<# 
 .Synopsis
    Takes an input of instructions and returns the resulting floor
 .Description
    Day One of adventofcode.com

    Santa has a ton of bad instructions in the following format:
        ( = Go up one floor
        ) = Go down one floor
    Get-InstructionResult gets the floor Santa will end up on
    Get-FirstBasement gets the instruction that will end Santa up in the basement the first time

 .Parameter instructions
    Instructions are a series of parenthesis

 .Example
    cat ".\input.txt" | Get-InstructionResult
#>

Function Get-InstructionResult{
    [cmdletbinding()]
    Param([Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [string]$instructions
    )
    process{
        $floor = 0
        foreach($instruct in $instructions.ToCharArray()){
            if($instruct -like "("){
                $floor = $floor + 1
            }elseif($instruct -like ")"){
                $floor = $floor - 1
            }
        }
        return $floor
         
    }
}

Function Get-FirstBasement{
    [cmdletbinding()]
    Param([Parameter(Mandatory=$true,ValueFromPipeline=$true)]
    [string]$instructions
    )
    process{
        $floor = 0
        $counter = 0
        foreach($instruct in $instructions.ToCharArray()){
            $counter = $counter + 1
            if($instruct -like "("){
                $floor = $floor + 1
            }elseif($instruct -like ")"){
                $floor = $floor - 1
            }
            if($floor -eq -1){
                return $counter
            }
        }
    }
}

Export-ModuleMember -Function Get-InstructionResult
Export-ModuleMember -Function Get-FirstBasement
﻿Import-Module "..\DayOne.psm1"

Describe "Get-InstructionResult"{
    It "Santa goes up two floors"{
        Get-InstructionResult -instructions "((" | Should Be 2
    }
    It "Santa goes up one, then comes back down"{
        Get-InstructionResult -instructions "()" | Should Be 0
    }
    It "Santa goes 3 deep"{
        Get-InstructionResult -instructions ")))" | Should Be -3
    }
    It "Santa follows some stupid directions"{
        Get-InstructionResult "((())()()()(((()))" | Should Be 2
    }
}

Describe "Get-FirstBasement"{
    It "Finds the first basement"{
        Get-FirstBasement -instructions ")" | Should Be 1
    }
    It "Doesn't find the first basement"{
        Get-FirstBasement -instructions "(" | Should Be
    }
    It "Finds the basement after a while"{
        Get-FirstBasement -instructions "((())()()()))(((()))" | Should Be 13
    }
    It "Doesn't find the basement after a while"{
        Get-FirstBasement -instructions "((())()()())(((()))" | Should Be
    }
}